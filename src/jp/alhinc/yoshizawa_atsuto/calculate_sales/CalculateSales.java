package jp.alhinc.yoshizawa_atsuto.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		Map<String, String> branchMap = new HashMap<>();
		Map<String, Long> salesMap = new HashMap<>();
		List<Integer> fileNameNum = new ArrayList<>();
		BufferedReader br = null;
		BufferedWriter bw = null;

		try {
			//支店データを抽出してbranchMapに格納
			File file = new File(args[0], "branch.lst");
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			br = new BufferedReader(new FileReader(file));
			String line;
			while((line = br.readLine()) != null) {
				String[] branches = line.split(",");

				//支店定義ファイルのフォーマットをチェック
				if(branches.length != 2 || !branches[0].matches("^[0-9]{3}")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				branchMap.put(branches[0], branches[1]);
				salesMap.put(branches[0], 0L);
			}

			//売上データ名をフィルタリングして抽出
			 FilenameFilter filter = new FilenameFilter() {
				 public boolean accept(File file, String str){
					 return str.matches("^\\d{8}.rcd$");
				 }
			 };

			 File[] files = new File(args[0]).listFiles(filter);
			//連番チェック
			 String fileName;
			 for(int i = 0; i < files.length; ++i) {
				 fileName = files[i].getName();
				 int fileNum = Integer.parseInt(fileName.replaceAll("[^0-9]",""));
				 fileNameNum.add(fileNum);
			 }
			 for(int k = 0; k < (files.length - 1); k++) {
				 if((fileNameNum.get(k) + 1) != fileNameNum.get(k + 1)) {
					 System.out.println("売上ファイル名が連番になっていません");
					 return;
				 }
			 }
			//売上ファイルの読込→計算のループ

			 for(int j = 0; j < files.length; ++j) {
				 //売上ファイルを読込→リスト
				 ArrayList<String> salesFile = new ArrayList<>();
				 br = new BufferedReader(new FileReader(files[j]));
				 String line01;
				 fileName = files[j].getName();
				 while((line01 = br.readLine()) != null) {
					 salesFile.add(line01);
				 }

				//売上ファイルの行数チェック
				 if(salesFile.size() != 2) {
					 System.out.println(fileName + "のフォーマットが不正です");
					 return;
				 }

				 //抽出した売上ファイルを計算(中にちょいちょいエラー処理有り)
				 String branchNum;
				 branchNum = salesFile.get(0);

				 //支店コードが該当するかチェック
				 if(!branchMap.containsKey(branchNum)) {
					 System.out.println(fileName + "の支店コードが不正です");
					 return;
				 }
				 long originalSale;
				 originalSale = salesMap.get(branchNum);
				 String stringSale;
				 stringSale = salesFile.get(1);
				 long addSale;
				 addSale = Long.parseLong(stringSale);

				 Long newValue;
				 newValue = originalSale + addSale ;

				 //売上合計金額の桁数チェック
				 if(newValue.toString().length() > 10) {
					 System.out.println("合計金額が10桁を超えました");
					 return;
				 }
				 salesMap.put(branchNum, newValue);
			 }

			 //集計結果出力
			 File newFile = new File(args[0], "branch.out");
			 newFile.createNewFile();
			 bw = new BufferedWriter(new FileWriter(newFile));
			 Iterator<String> key_itr = branchMap.keySet().iterator();
			 while(key_itr.hasNext()) {
				 String branchNum01 = (String)key_itr.next();
				 bw.write(branchNum01 + "," + branchMap.get(branchNum01) + "," + salesMap.get(branchNum01));
				 bw.newLine();
			 }
		} catch(IOException e) {
			//tryで例外が発生した時に通知する
			System.out.println("予期せぬエラーが発生しました。");
		} finally {
			//例外をキャッチしてもしなくても行う処理
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
				}
			}
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
				}
			}
		}
	}
}